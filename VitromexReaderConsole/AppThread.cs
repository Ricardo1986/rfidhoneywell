﻿using MetalsaReaderConsole;
using System.Threading;
using MetalsaReaderConsole.Components;

namespace MetalsaReaderConsole
{
    public class AppThread
    {
        public bool _bKill = false;
        public bool _appIsRunning = false;
        public string _sVersion = "";

        RFIDReaderClass _myReader = new RFIDReaderClass();

        public void RunApp()
        {
            ConfigurationFile.LoadConfigurationFile();

            System.Console.WriteLine("Terminó la lectura de las configuraciones");
            bool bStatus = false;
            bool bUseDebug = true;

            _bKill = false;

            //connection for talking to BRI server via the IDL driver
            //string sURL = "TCP://" + "127.0.0.1" + ":2189";  //localhost connection in IF1/IF2.

            string sURL =  bUseDebug ? "TCP://" + ConfigurationFile._ipReader + ":2189" : "TCP://" + "127.0.0.1" + ":2189";   //when I'm running code on my PC

            System.Console.WriteLine(sURL);

            //if (bUseDebug)
            //    bStatus = _myReader.CreateReaderWithIDLDebugging("TCP://" + "127.0.0.1" + ":2189");  //use idl debug
            //else
                bStatus = _myReader.ConectionWithReader(sURL);  //no idl debugging

            //_myReader.WriteGpio();

            if (bStatus)
            {
                System.Console.WriteLine("reader open and ready");

                _appIsRunning = true;

                while (_appIsRunning)
                {
                    //just sleep
                    Thread.Sleep(1000);
                }
            }

            if (_myReader != null) { _myReader.CloseReader(); _myReader = null; }
            _bKill = true;
        }
    }
}
