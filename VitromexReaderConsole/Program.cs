﻿using System.Threading;

namespace MetalsaReaderConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            AppThread oAlpha = new AppThread();

            Thread oThread = new Thread(new ThreadStart(oAlpha.RunApp));

            //set source code version for display
            oAlpha._sVersion = "1.7";

            System.Console.WriteLine("Console Application Ver 1.7 Starting...");

            oThread.Start();

            while (!oThread.IsAlive)
                Thread.Sleep(1000);

                while (!oAlpha._bKill)
                    Thread.Sleep(1000);

                    System.Console.WriteLine("Console Application Closing...");

            oThread.Abort();

            oThread.Join();

            System.Console.WriteLine("Goodbye");
        }
    }
}
