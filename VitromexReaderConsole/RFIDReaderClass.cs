﻿using Intermec.DataCollection.RFID;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices;
using System.IO;
using System.Net;
using System.Security.Permissions;
using System.Text.RegularExpressions;
using System.Timers;
using Metalsa.Desktop.Components;
using MetalsaReaderConsole.Components;
using System.Xml;
using System.Text;
using MetalsaReaderConsole.Clases;

namespace MetalsaReaderConsole
{
    public class RFIDReaderClass
    {
        private List<string> tagsRead = new List<string>();
        private List<string> lastTagsRead = new List<string>();
        private string sensorDireccion;
        private static Timer _timer;
        private static Timer _timerLastTagsRead;
        private static Timer _timerStackLight;
        private BRIReader _brdr = null;
        private bool bIsReading;
        private string _xmlGenerated;
        private string _fileName = string.Empty;
        private static bool _finishTime = true;
        private System.Object lockThis = new System.Object();
        private FtpClient ClientFTP;
        private bool BanderaParpadeo=true;

        private bool borrarTags = false;


        private void _timerStackLight_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                //Console.WriteLine("STACKLIGHT EVENTO INICIADO");
                _timerStackLight.Stop();
                LeerArchivosRuta();
                _timerStackLight.Start();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Message: " + ex.Message + " Inner:" + ex.InnerException);
            }
        }

        public bool ConectionWithReader(string sURL)
        {
            //ClientFTP = new FtpClient(ConfigurationFile._FTPhostIP, ConfigurationFile._FTPuserName, ConfigurationFile._FTPpassword);
            //Console.WriteLine("FTP Host: " + ConfigurationFile._FTPhostIP + " User:" + ConfigurationFile._FTPuserName + " Password:" + ConfigurationFile._FTPpassword);

            string sMsg = null;
            bool bStatus = false;

            System.Console.WriteLine("Connecting to reader...");
            
            try
            {
                //Create reader connection no IDL debugging is enabled.
                _brdr = new BRIReader( null, sURL, 22000, 2000);

                _timer = new Timer();
                //_timer.Interval = 5000;
                string tiempo = "El tiempo del verificación(timer) es: " + ConfigurationFile._timerTime;
                System.Console.WriteLine(tiempo);
                _timer.Interval = ConfigurationFile._timerTime;
                _timer.Elapsed += _timer_Elapsed;
                WriteGpioOFF();
                //_timerStackLight = new Timer();
                //_timerStackLight.Interval = 1000;
                //_timerStackLight.Elapsed += _timerStackLight_Elapsed;
                //_timerStackLight.Start();

                _timerLastTagsRead = new Timer();
                _timerLastTagsRead.Interval  = ConfigurationFile._timerTime;
                _timerLastTagsRead.Elapsed += _timerLastTagsRead_Elapsed;
                _timerLastTagsRead.Start();

                sMsg = _brdr.Execute("TRIGGER RESET");
                if (sMsg.IndexOf("OK>") >= 0)
                {
                    AddEventHandlers();
                    SetAttribs();

                    //only create triggers if you are using motion sensors
                    //to start/stop the read process
                    InitialiceSensor();

                    //if you plan to use motion sensors remove this next line.
                    //StartReadingTags();

                    bStatus = true;
                }
            }
            catch (BasicReaderException eBRI)
            {
                bStatus = false;
                System.Console.WriteLine("Error ConectionWithReader " + eBRI.Message);
            }

            return bStatus;
        }

        private void _timerLastTagsRead_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                lastTagsRead.Clear();
            }
            catch (Exception ex)
            {
                System.Console.WriteLine("lastTagsRead.Clear(); " + ex.Message);
            }
        }

        private void timer_Elapsed()
        {
            try
            {

                bIsReading = false;
                StopReadingTags();

                Console.WriteLine(string.Format("timer_Elapsed: <{0}>", !ReadContainsLastTags()));

                if (!ReadContainsLastTags())
                {
                    Console.WriteLine(string.Format("TIMER STOP READING IP: <{0}>", ConfigurationFile._ipReader));
                    Console.WriteLine(string.Format("Dirección Sensor: <{0}>", sensorDireccion));
                    Console.WriteLine(string.Format("{0}", sensorDireccion));
                    Console.WriteLine("Antes de leer los tags");
                    LoadTagList((Metalsa.Desktop.Components.Enums.TagDirection)Enum.Parse(typeof(Metalsa.Desktop.Components.Enums.TagDirection), sensorDireccion));

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format(@"Inner Exception:{0}. \n Message Error:{1}. \n Source:{2} \n StackTrace: {3}", ex.InnerException, ex.Message, ex.Source, ex.StackTrace));
            }
            finally
            {
                _finishTime = true;
            }
        }


        private void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                Timer currentTimer = (Timer)sender;

                _timer.Enabled = false;
                _timer.Stop();
                bIsReading = false;
                StopReadingTags();

                if (!ReadContainsLastTags())
                {
                    Console.WriteLine(string.Format("TIMER STOP READING IP: <{0}>", ConfigurationFile._ipReader));
                    Console.WriteLine(string.Format("Dirección Sensor: <{0}>", sensorDireccion));
                    Console.WriteLine(string.Format("{0}", sensorDireccion));
                    Console.WriteLine("Antes de leer los tags");
                    LoadTagList((Metalsa.Desktop.Components.Enums.TagDirection)Enum.Parse(typeof(Metalsa.Desktop.Components.Enums.TagDirection), sensorDireccion));

                }
                else {
                    //Se aregó el 31/10/2017
                    //tagsRead.Clear();
                    //lastTagsRead.Clear();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format(@"Inner Exception:{0}. \n Message Error:{1}. \n Source:{2} \n StackTrace: {3}", ex.InnerException, ex.Message, ex.Source, ex.StackTrace));
            }
            finally {
                _finishTime = true;
            }
        }

        public void SendFPT() {
            try
            {
                var ele = "smb:/" + System.Windows.Forms.Application.StartupPath + "/MetalsaSchema.xml";
                if (!string.IsNullOrEmpty(_xmlGenerated))
                {
                    ClientFTP.UPLOAD3(_xmlGenerated, _xmlGenerated);
                    _xmlGenerated = string.Empty;
                    Console.WriteLine("Se envío el archivo");
                }
                else {
                    Console.WriteLine("No contiene datos el archivo y no se envío");
                }

            }
            catch (Exception ex) {
                Console.WriteLine(string.Format(@"Inner Exception:{0}. \n Message Error:{1}. \n Source:{2} \n StackTrace: {3}", ex.InnerException, ex.Message, ex.Source, ex.StackTrace));
            }

        }

        private bool ReadContainsLastTags()
        {
            bool result = false;

            foreach (var item in tagsRead)
            {
                if (lastTagsRead.Contains(item))
                {
                    result = true;
                    break;
                }
            }

            return result;
        }

       //Prender el led verde 3 segundos, apagarlo y prender el amarillo
        public void WriteGpioGreen()
        {
            string r = null;
            Console.WriteLine("Prende el verde  y espera 3 segundos");
            r = _brdr.Execute("writegpo 2 OFF");
            System.Threading.Thread.Sleep(3000);
            r = _brdr.Execute("writegpo 2 ON");
        }

        //Prender el led amarillo 3 segundos, apagarlo y prender el amarillo
        public void WriteGpioYellow()
        {
            string r = null;
            Console.WriteLine("Prende el amarillo  y espera 3 segundos");
            r = _brdr.Execute("writegpo 1 OFF");
            r = _brdr.Execute("writegpo 2 OFF");
            System.Threading.Thread.Sleep(3000);
            r = _brdr.Execute("writegpo 1 ON");
            r = _brdr.Execute("writegpo 2 ON");
        }

        //Prender el led rojo 3 segundos, apagarlo y prender el amarillo
        public void WriteGpioRed()
        {
            string r = null;
            Console.WriteLine("Prende el led rojo y espera 1 segundos");
            r = _brdr.Execute("writegpo 1 OFF");
            System.Threading.Thread.Sleep(1000);
            r = _brdr.Execute("writegpo 1 ON");
            Console.WriteLine("Agaga el led rojo");
            System.Threading.Thread.Sleep(1000);
            Console.WriteLine("Prende el led rojo y espera 1 segundos");
            r = _brdr.Execute("writegpo 1 OFF");
            System.Threading.Thread.Sleep(1000);
            r = _brdr.Execute("writegpo 1 ON");
            Console.WriteLine("Agaga el led rojo");
            System.Threading.Thread.Sleep(1000);



            Console.WriteLine("Prende el led rojo y espera 1 segundos");
            r = _brdr.Execute("writegpo 1 OFF");
            System.Threading.Thread.Sleep(1000);
            r = _brdr.Execute("writegpo 1 ON");
            Console.WriteLine("Agaga el led rojo");
        }

        //Prender el led rojo 3 segundos, apagarlo y prender el amarillo
        public void WriteGpioOFF()
        {
            string r = null;
            r = _brdr.Execute("writegpo 2 ON");
            r = _brdr.Execute("writegpo 1 ON");
            Console.WriteLine("Apaga todos los leds ");
        }

        public void Parpadeo() {
            while (BanderaParpadeo) {
                string r = null;
                Console.WriteLine("Prende el let rojo y espera 3 segundos");
                r = _brdr.Execute("writegpo 1 OFF");
                System.Threading.Thread.Sleep(1000);
                r = _brdr.Execute("writegpo 1 ON");
                Console.WriteLine("Agaga el led rojo");
            }
        }



        public void WriteGpio()
        {
            string r = null;
            try
            {
                //r = _brdr.Execute("writegpo 2 off");

                r = _brdr.Execute("writegpo 1 on");
                Console.WriteLine(string.Format("writegpo 1 on {0}",r));

                r = _brdr.Execute("writegpo 2 on");
                Console.WriteLine(string.Format("writegpo 2 on {0}", r));
                r = _brdr.Execute("READGPI");
                Console.WriteLine(string.Format("READGPI {0}", r));
                r = _brdr.Execute("writegpo 3 on");
                Console.WriteLine(string.Format("writegpo 3 on {0}", r));
            }
            catch (Exception ex)
            {

                Console.WriteLine(string.Format(@"Inner Exception:{0}. \n Message Error:{1}. \n Source:{2} \n StackTrace: {3}", ex.InnerException, ex.Message, ex.Source, ex.StackTrace));
            }
        }

        private void AddEventHandlers()
        {
            _brdr.EventHandlerAntennaFault += new AntennaFault_EventHandler(Brdr_EventHandlerAntennaFault);
            _brdr.EventHandlerTag += new Tag_EventHandlerAdv(Brdr_EventHandlerTag);
            _brdr.EventHandlerThermal += new Thermal_EventHandler(Brdr_EventHandlerThermal);
            _brdr.EventHandlerGPIO += new GPIO_EventHandlerAdv( brdr_EventHandlerGPIO);
        }

        private void Brdr_EventHandlerThermal(object sender, Thermal_EventArgs EvtArgs)
        {
            Console.WriteLine("Temperatura de IF2: " + EvtArgs.Temperature + " Estado de IF2: " + EvtArgs.StateString);
        }

        private void Brdr_EventHandlerTag(object sender, EVTADV_Tag_EventArgs EvtArgs)
        {
            Console.WriteLine("Brdr_EventHandlerTag ");
            try
            {
                
                if (! string.IsNullOrEmpty(EvtArgs.Tag.ToString())){

                    if (EvtArgs.Tag.ToString().Length > 0)
                    {
                        Console.WriteLine("En Brdr_EventHandlerTag:" + EvtArgs.Tag.ToString());
                        //var tag = ConvertHexToString(EvtArgs.Tag.ToString());

                        var tag = EvtArgs.Tag.ToString();

                        if (!tagsRead.Contains(tag))
                        {
                            Console.WriteLine(string.Format("Se agregará {0}", tag));
                            tagsRead.Add(string.Format("{0}", tag));
                        }
                    }
                    else {
                        Console.WriteLine("el tag estaba vacio " + (EvtArgs.Tag.ToString()));
                    }
                    
                }
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format(@"Inner Exception:{0}. \n Message Error:{1}. \n Source:{2} \n StackTrace: {3}", ex.InnerException, ex.Message, ex.Source, ex.StackTrace));
            }
        }

        private void Brdr_EventHandlerAntennaFault(object sender, AntennaFault_EventArgs EvtArgs)
        {
            Console.WriteLine(string.Format ("Error en antena: {0} error {1}" , EvtArgs.AntennaID, EvtArgs  ));
        }

        void brdr_EventHandlerGPIO(object sender, EVTADV_GPIO_EventArgs EvtArgs)
        {
            
            Console.WriteLine(string.Format("brdr_EventHandlerGPIO _finishTime {0}", _finishTime));
            //lock (lockThis)
            //{

            try
            {
                if (_finishTime)
                {
                    _finishTime = false;
                    //HWPROD  
                    string trigname = EvtArgs.TriggerNameString;
                    Console.WriteLine("Trigname: " + trigname);

                    if (trigname.StartsWith("SensorAON"))
                    {
                        Console.WriteLine("bIsReading: " + !bIsReading);
                        if (!bIsReading)
                        {
                            StartReadingTags();
                            System.Threading.Thread.Sleep(ConfigurationFile._timerTime);
                            sensorDireccion = "sale";
                            timer_Elapsed();
                            _timer.Start();


                            Console.WriteLine(string.Format("---> A  START READING IP SensorAON: <{0}> Dirección {1}", ConfigurationFile._ipReader, sensorDireccion));

                            bIsReading = true;
                        }
                    }
                    if (trigname.StartsWith("SensorBON"))
                    {
                        Console.WriteLine("bIsReading: " + !bIsReading);
                        if (!bIsReading)
                        {
                            Console.WriteLine("Inicia la lectura de los tags ");
                            StartReadingTags();
                            System.Threading.Thread.Sleep(ConfigurationFile._timerTime);
                            sensorDireccion = "entra";
                            timer_Elapsed();
                            _timer.Start();


                            Console.WriteLine(string.Format("---> B  START READING IP SensorBON: <{0}>  Dirección {1}", ConfigurationFile._ipReader, sensorDireccion));

                            bIsReading = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(string.Format(@"Inner Exception:{0}. \n Message Error:{1}. \n Source:{2} \n StackTrace: {3}", ex.InnerException, ex.Message, ex.Source, ex.StackTrace));
            }
            //finally {
            //    WriteGpioGreen();
            //}
            //}

        }

        public void InitialiceSensor()
        {
            //Console.WriteLine(_brdr.Execute("TRIGGER \"SensorAON\" GPIOEDGE 1 1 FILTER 2000"));
            //Console.WriteLine(_brdr.Execute("TRIGGER \"SensorBON\" GPIOEDGE 2 2 FILTER 2000"));
            //Console.WriteLine(_brdr.Execute("TRIGGER \"SensorAON\" GPIOEDGE 1 1 FILTER 500"));
            //Console.WriteLine(_brdr.Execute("TRIGGER \"SensorBON\" GPIOEDGE 2 2 FILTER 500"));
            _brdr.Execute("TRIGGER \"SensorAON\" GPIOEDGE 1 0 FILTER 2000");
            _brdr.Execute("TRIGGER \"SensorBON\" GPIOEDGE 2 0 FILTER 2000");
            System.Console.WriteLine("Termina inicializar sensores");

        }

        public void StopReadingTags()
        {
            System.Console.WriteLine("Lectura de tags terminada...");
            _brdr.StopReadingTags();
            //_brdr.Execute("writegpo 1 off");
        }


        private void LoadTagList(Metalsa.Desktop.Components.Enums.TagDirection direcction)
        {
            Console.WriteLine(string.Format("Cantidad a leer {0}", tagsRead.Count));
            if (tagsRead.Count > 0)
            {
                List<string> tmpTags = new List<string>();
                String Listado = string.Empty;

                for (int i = 0; i < tagsRead.Count; i++)
                {
                        //var tagID = ConvertHexToString(tagsRead[i]);
                        if (!tmpTags.Contains(tagsRead[i]))
                        {
                            tmpTags.Add(tagsRead[i]);
                            Console.WriteLine("Se agregará:  " + tagsRead[i]);
                        if (!string.IsNullOrEmpty(Listado))
                        {
                            Listado = string.Format("{0},{1}", Listado, tagsRead[i]);
                        }
                        else {
                            Listado = string.Format("{1}", Listado, tagsRead[i]);
                        }
                           
                        }
                }
                Console.WriteLine(string.Format("tmpTags {0}", tmpTags.Count));
                if (tmpTags.Count > 0)
                {
                    try
                    {
                        Console.WriteLine("Se iniciará el servicio WEB" );
                        //var MiCliente = new ServicioWeb();
                        Console.WriteLine("Terminó iniciar servicio WEB");
                        int direccion = (int)direcction;

                       
                        Console.WriteLine("Dirección:  " + direccion);
                        Console.WriteLine("IP READER :  " + ConfigurationFile._ipReader);
                        Console.WriteLine("Listado :  " + Listado);
                        Console.WriteLine("URL de archivo :  " + ConfigurationFile._webService);
                        ServicioWeb .Url = ConfigurationFile._webService;
                        //MiCliente.MiServicio.Url = ConfigurationFile._webService;
                        Console.WriteLine("URL :  " + ServicioWeb.Url);
                        ServicioWeb.MiServicio.SetData(ConfigurationFile._ipReader, direccion, Listado);
                        Console.WriteLine("Despues de mandar datos " );
                        BanderaParpadeo = false;
                        WriteGpioGreen();
                        //WriteGpioOFF();
                    }
                    catch (Exception e)
                    {
                       
                        WriteGpioRed();
                        Console.WriteLine("{0} Exception caught. {1}", e,e.StackTrace );
                    }

                    //Console.WriteLine("Generando XML IP " + ConfigurationFile._ipReader);
                    //_xmlGenerated = XMLGenerator.GenerateXml(ConfigurationFile._ipReader, tmpTags, direcction);

                    foreach (var item in tagsRead)
                    {
                        lastTagsRead.Add(item);
                    }

                    tmpTags = null;
                    if (borrarTags)
                    {
                        tagsRead.Clear();
                    }
                    else {
                        borrarTags = true;
                    }

                    
                }
            }
        } 

        private void SendDatatoWebService()
        {
            try
            {
                if (!string.IsNullOrEmpty(_xmlGenerated))
                {
                    // Create a new WebClient instance.
                    WebClient myWebClient = new WebClient();

                    // Apply ASCII Encoding to obtain the string as a byte array.
                    byte[] postArray = Encoding.ASCII.GetBytes(_xmlGenerated);
                    Console.WriteLine("Uploading to {0} ...", ConfigurationFile._webService);
                    myWebClient.Headers.Add("Content-Type", "application/x-www-form-urlencoded");

                    Console.WriteLine("Creó header...");
                    //UploadData implicitly sets HTTP POST as the request method.
                    byte[] responseArray = myWebClient.UploadData (ConfigurationFile._webService, postArray);

                    // Decode and display the response.
                    Console.WriteLine("\nResponse received was :{0}", Encoding.ASCII.GetString(responseArray));
                }
            }
            catch (Exception ex) {
                Console.WriteLine(string.Format("ERROR|{0}|{1}", ex.InnerException, ex.Message));
            }
           
         }
        

        private void CallWebService()
        {
            try {
                
                    if (!string.IsNullOrEmpty(_xmlGenerated))
                    {
                        //using (var webClient = new WebClient())
                        using (var webClient = new TimedWebClient())
                        {
                            webClient.Timeout = 60000;
                            var serviceUri = ConfigurationFile._webService;
                            string schema = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";

                            webClient.Headers[HttpRequestHeader.Accept] = "application/ json";

                            var uri = new Uri(string.Format("{0}{1}", serviceUri, _xmlGenerated));
                            Console.WriteLine(string.Format("{0}{1}", serviceUri, _xmlGenerated));

                            string result = webClient.DownloadString(uri);

                            if (!string.IsNullOrEmpty(result))
                            {
                                Console.WriteLine("XML ENVIADO AL WEB SERVICE");
                                Console.WriteLine(result);
                                //CrearLogXML(string.Format("{1}{0}", _xmlGenerated, schema));
                                _xmlGenerated = string.Empty;
                                WriteGpioGreen();
                            }
                            else
                            {
                                //_xmlGenerated = string.Empty;
                                WriteGpioRed();
                                Console.WriteLine("WEB SERVICE SIN ACCESO");
                            }
                        }
                    }
                    else
                    {
                        //WriteGpioRed();
                        Console.WriteLine("NO HAY TAGS PARA GENERAR XML");
                    }
            }
            catch(Exception ex)
            {
                Console.WriteLine(string.Format("ERROR al enviar al servicio web|{0}|{1}", ex.InnerException, ex.Message));
                WriteGpioRed();
            }
            
        }

        public void CloseReader()
        {
            //delete macros and triggers
            //dispose of IDL reader class
            string sMsg = null;

            if (_brdr != null)
            {
                //delete any triggers you created.
                sMsg = _brdr.Execute("TRIGGER RESET");

                //close reader connection and dispose of object
                _brdr.Dispose();
                _brdr = null;
            }
        }

        public void SetAttribs()
        {
            string rsp = null;
            string line = string.Empty;
            Console.WriteLine(string.Format(@"{0}/{1}.txt", ConfigurationFile._pathReaderAttributes, ConfigurationFile._ipReader));
            System.IO.StreamReader file = new System.IO.StreamReader(string.Format(@"{0}/{1}.txt", ConfigurationFile._pathReaderAttributes, ConfigurationFile._ipReader));

            while ((line = file.ReadLine()) != null)
            {
                try
                {
                    rsp = _brdr.Execute(line);

                    if (rsp.IndexOf("ERR") >= 0)
                        System.Console.WriteLine(line + " -> Warning ERROR!!! " + rsp);
                    else
                        System.Console.WriteLine(line + " -> " + rsp);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(string.Format("ERROR|{0}|{1}", ex.InnerException, ex.Message));
                }

            }

            file.Close();
        }

        public void LeerArchivosRuta()
        {
            //Console.WriteLine("Creando instancia");
            //UnidadRedUtil unidaad = new UnidadRedUtil();
            //unidaad.MapResource("", ConfigurationFile._pathReaderStackLight);
            string ruta = ConfigurationFile._pathReaderStackLight;
            //const string ruta = @"\\192.168.0.112\readers";
            Console.WriteLine("Instancia creada");
            Console.WriteLine(" La ruta es: "+ruta);
            if (!Directory.Exists(ruta))
            {
                Console.WriteLine("Despues de verificar ruta");
                Directory.CreateDirectory(ruta);
                Console.WriteLine("Despues de crear ruta");
            }

            Regex nombreArchivo = new Regex("^[0-9]{14}[.](txt)$");
            DirectoryInfo info = new DirectoryInfo(ruta);
            //Console.WriteLine("Antes foreach");
            foreach (var files in info.GetFiles("*.txt"))
            {
                if ((nombreArchivo.IsMatch(files.Name)) && ((((files.Name).ToString()).Substring(0, 8)) == (DateTime.Now).ToString("yyyyMMdd")))
                {
                    _fileName = files.Name;
                    LeerArchivoServicio(files.FullName, files.Name);
                }
                else
                {
                    Console.WriteLine("Sin archivos");
                }
            }

            //unidaad.UnMapResource("");
        }

        public void LeerArchivoServicio(string archivo, string fileName)
        {
            StreamReader lector = new StreamReader(archivo);
            string reader = string.Empty;

            try
            {

                string instruccion;
                String[] partes;
                while ((instruccion = lector.ReadLine()) != null)
                {
                    partes = instruccion.Split('|');

                    for (int a = 0; a <= (partes.Length) - 1; a++)
                    {
                        reader = partes[0];
                        string orden = partes[1];
                        if (a == (partes.Length) - 1)
                        {
                            if (reader == ConfigurationFile._ipReader)
                            {
                                EjecutarAccionReader(orden);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("ERROR|{0}|{1}", ex.InnerException, ex.Message), _fileName);
            }
            finally
            {
                lector.Close();

                if (reader == ConfigurationFile._ipReader)
                {
                    FileInfo infoArchivo = new FileInfo(archivo);
                    string nombreNvo = (infoArchivo.Name).Substring(0, 14) + "-leido2";
                    string ruta = infoArchivo.DirectoryName;
                    File.Move(ruta + "/" + infoArchivo.Name, ruta + "/" + nombreNvo + ".txt");
                }
            }
        }

        public bool EjecutarAccionReader(string ordenSemaforo)
        {
            bool bStatus = false;
            string sMsg = null;

            if ((_brdr.Execute("PING")).Trim().Equals("OK>"))
            {
                switch (ordenSemaforo)
                {
                    case "ON":
                        _brdr.Execute("writegpo 2 ON");
                        bStatus = true;
                        break;
                    case "OFF":
                        _brdr.Execute("writegpo 1 OFF");
                        _brdr.Execute("writegpo 2 OFF");
                        _brdr.Execute("writegpo 3 OFF");
                        bStatus = true;
                        break;
                    case "TEST":
                        _brdr.Execute("ATTRIB ANTS");
                        _brdr.Execute("writegpo 2 ON");
                        _brdr.Execute("writegpo 2 OFF");
                        bStatus = true;
                        break;
                    case "GREEN":
                        _brdr.Execute("writegpo 1 ON");
                        bStatus = true;
                        break;
                    case "RED":
                        sMsg = _brdr.Execute("writegpo 3 ON");
                        bStatus = true;
                        break;
                }

                if (bStatus == true)
                {
                    Console.WriteLine("COMANDO EJECUTADO");
                }
                else
                {
                    Console.WriteLine("EEROR AL ENVIAR ORDEN AL LED");
                }
            }
            else
            {
                Console.WriteLine("1: VERIFICAR QUE EL IF2 ESTE ENCENDIDO Y/O EN LÍNEA!");
            }

            return bStatus;
        }

        private void StartReadingTags()
        {
            
            _brdr.StartReadingTags(null, "\"" + ConfigurationFile._ipReader + "\"", BRIReader.TagReportOptions.EVENT);
            Console.WriteLine(string.Format("/{0}/{1}", ConfigurationFile._ipReader, BRIReader.TagReportOptions.EVENT));
            //_brdr.Execute("writegpo 1 on");
        }
    }
}
